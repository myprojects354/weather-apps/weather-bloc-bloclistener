import 'package:flutter/material.dart';

class LabelWhoCreated extends StatelessWidget {
  const LabelWhoCreated({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'assets/images/logo_codebridge.png',
          width: 30,
          height: 30,
        ),
        const Text(
          'Created by Viktor Lutsenko \nfor Codebridge',
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}

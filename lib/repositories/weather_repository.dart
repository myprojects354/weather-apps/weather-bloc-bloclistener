import 'package:flutter/foundation.dart';
import 'package:weather_bloc_listener/exception/weather_exception.dart';
import 'package:weather_bloc_listener/models/custom_error.dart';
import 'package:weather_bloc_listener/models/direct_geocoding.dart';
import 'package:weather_bloc_listener/models/weather.dart';
import 'package:weather_bloc_listener/services/weather_api_services.dart';

class WeatherRepository {
  final WeatherApiServices weatherApiServices;
  WeatherRepository({
    required this.weatherApiServices,
  });

  Future<Weather> fetchWeather(String city) async {
    try {
      final DirectGeocoding directGeocoding =
          await weatherApiServices.getDirectGeocoding(city);
      if (kDebugMode) {
        print('directGeocoding: $directGeocoding');
      }

      final Weather tempWeather =
          await weatherApiServices.getWeather(directGeocoding);

      final Weather weather = tempWeather.copyWith(
        name: directGeocoding.name,
        country: directGeocoding.country,
      );

      return weather;
    } on WeatherException catch (e) {
      throw CustomError(errMsg: e.message);
    } catch (e) {
      throw CustomError(errMsg: e.toString());
    }
  }
}
